import com.k_team.binary.Binary;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * Tests the XOR Operation
 *
 * @author Yago Carballo
 * @version June 9, 2013
 */
public class TestOperationLEFT {
    @Test
    public void checkOperationLEFT () throws Exception {
        Binary num1 = new Binary("00000001");
        assertEquals("00000001 LEFT 3 must be 00001000", 0b00001000, Binary.left(num1, 3).getNumber());


        num1 = new Binary(0b00111100);
        assertEquals("00111100 LEFT 2 must be 11110000", 0b11110000, Binary.left(num1, 2).getNumber());
    }
}
