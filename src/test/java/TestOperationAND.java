import com.k_team.binary.Binary;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * Tests the XOR Operation
 *
 * @author Yago Carballo
 * @version June 9, 2013
 */
public class TestOperationAND {
    @Test
    public void checkOperationAND () throws Exception {
        Binary num1 = new Binary("00000001");
        Binary num2 = new Binary("00000011");
        assertEquals("00000001 AND 00000011 must be 00000001", 0b00000001, Binary.and(num1, num2).getNumber());


        num1 = new Binary(0b00111100);
        num2 = new Binary(0b00001101);
        assertEquals("00111100 AND 00001101 must be 00001100", 0b00001100, Binary.and(num1, num2).getNumber());
    }
}
