import com.k_team.binary.Binary;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * Tests the XOR Operation
 *
 * @author Yago Carballo
 * @version June 9, 2013
 */
public class TestOperationFLIP {
    @Test
    public void checkOperationFLIP () throws Exception {
        // TODO: Understand FLIP

        Binary num1 = new Binary("00000001");
//        assertEquals("FLIP 00000001 must be 11111110", 0b11111101, Binary.flip(num1).getNumber());
        assertEquals("FLIP 00000001 must be 11111110", -2, Binary.flip(num1).getNumber());


        num1 = new Binary(0b00111100);
//        assertEquals("FLIP 00111100 must be 11000011", 0b11000011, Binary.flip(num1).getNumber());
        assertEquals("FLIP 00111100 must be 11000011", -61, Binary.flip(num1).getNumber());
    }
}
