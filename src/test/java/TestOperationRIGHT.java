import com.k_team.binary.Binary;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * Tests the XOR Operation
 *
 * @author Yago Carballo
 * @version June 9, 2013
 */
public class TestOperationRIGHT {
    @Test
    public void checkOperationRIGHT () throws Exception {
        Binary num1 = new Binary("00000001");
        assertEquals("00000001 RIGHT 3 must be 00000001", 0b00000000, Binary.right(num1, 3).getNumber());


        num1 = new Binary(0b00111100);
        assertEquals("00111100 RIGHT 2 must be 00001111", 0b00001111, Binary.right(num1, 2).getNumber());
    }
}
