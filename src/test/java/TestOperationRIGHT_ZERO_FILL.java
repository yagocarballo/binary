import com.k_team.binary.Binary;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * Tests the XOR Operation
 *
 * @author Yago Carballo
 * @version June 9, 2013
 */
public class TestOperationRIGHT_ZERO_FILL {
    @Test
    public void checkOperationRIGHT_ZERO_FILL () throws Exception {
        Binary num1 = new Binary("00000001");
        assertEquals("00000001 RIGHT_ZERO_FILL 3 must be 00000001", 0b00000000, Binary.rightZeroFill(num1, 3).getNumber());


        num1 = new Binary(0b00111100);
        assertEquals("00111100 RIGHT_ZERO_FILL 2 must be 00001111", 0b00001111, Binary.rightZeroFill(num1, 2).getNumber());
    }
}
