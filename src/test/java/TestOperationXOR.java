import com.k_team.binary.Binary;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * Tests the XOR Operation
 *
 * @author Yago Carballo
 * @version June 9, 2013
 */
public class TestOperationXOR {
    @Test
    public void checkOperationXOR () throws Exception {
        Binary num1 = new Binary("00000001");
        Binary num2 = new Binary("00000011");
        assertEquals("00000001 XOR 00000011 must be 00000010", 0b00000010, Binary.xor(num1, num2).getNumber());


        num1 = new Binary(0b00111100);
        num2 = new Binary(0b00001101);
        assertEquals("00111100 XOR 00001101 must be 00110001", 0b00110001, Binary.xor(num1, num2).getNumber());
    }
}
