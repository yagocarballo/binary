package com.k_team.binary;

/**
 * Class to work with Binaries
 *
 * @author Yago Carballo
 * @version June 9, 2013
 */
public class Binary {
    private int number = 0b00000000;

    /**
     * Creates an empty Binary
     *
     * Default Value: 00000000
     */
    public Binary () {}

    /**
     * Creates a Binary with a String
     *
     * @param number (String) value of the binary
     */
    public Binary (String number) {
        this.setNumber(Integer.parseInt(number, 2));
    }

    /**
     * Creates a Binary with an int value
     *
     * IMPORTANT!!: put 0b or 0B before the number to set it as binary
     *
     * @param num (int) value of the binary (Start with 0b or 0B)
     */
    public Binary (int num) {
        this.setNumber(num);
    }

    /**
     * Binary XOR Operator copies the bit if it is set in one operand but not both.
     *
     * @param num1 (Binary) The first Binary
     * @param num2 (Binary) The second Binary
     * @return (Binary)
     */
    public static Binary xor (Binary num1, Binary num2) {
        return new Binary(num1.getNumber() ^ num2.getNumber());
    }

    /**
     * Binary OR Operator copies a bit if it exists in either operand.
     *
     * @param num1 (Binary) The first Binary
     * @param num2 (Binary) The second Binary
     * @return (Binary)
     */
    public static Binary or (Binary num1, Binary num2) {
        return new Binary(num1.getNumber() | num2.getNumber());
    }

    /**
     * Binary Left Shift Operator. The left operands value is moved left
     * by the number of bits specified by the right operand.
     *
     * @param num1 (Binary) The first Binary
     * @param pos (int) Positions to move Left
     * @return (Binary)
     */
    public static Binary left (Binary num1, int pos) {
        return new Binary(num1.getNumber() << pos);
    }

    /**
     * Binary Right Shift Operator. The left operands value is moved
     * right by the number of bits specified by the right operand.
     *
     * @param num1 (Binary) The first Binary
     * @param pos (int) Positions to move Right
     * @return (Binary)
     */
    public static Binary right (Binary num1, int pos) {
        return new Binary(num1.getNumber() >> pos);
    }

    /**
     * Shift right zero fill operator. The left operands value
     * is moved right by the number of bits specified by the right
     * operand and shifted values are filled up with zeros.
     *
     * @param num1 (Binary) The first Binary
     * @param pos (int) Positions to move Right
     * @return (Binary)
     */
    public static Binary rightZeroFill (Binary num1, int pos) {
        return new Binary(num1.getNumber() >>> pos);
    }

    /**
     * Binary AND Operator copies a bit to the result if it exists in both operands.
     *
     * @param num1 (Binary) The first Binary
     * @param num2 (Binary) The second Binary
     * @return (Binary)
     */
    public static Binary and (Binary num1, Binary num2) {
        return new Binary(num1.getNumber() & num2.getNumber());
    }

    /**
     * Binary Ones Complement Operator is unary and has the effect of 'flipping' bits.
     *
     * @param num1 (Binary) The first Binary
     * @return (Binary)
     */
    public static Binary flip (Binary num1) {
        return new Binary(~num1.getNumber());
    }

    /**
     * Formats the Binary as String with the Size Specified.
     *
     * @param num (Binary)  the Binary to Format.
     * @param size (int)    the size to display the Binary.
     * @return (String)
     */
    public static String format (Binary num, int size) {
        String binary = Integer.toBinaryString(num.getNumber());

        for (int i = binary.length(); i<size; i++)
            binary = '0' + binary;

        return String.format("%s", binary);
    }

    /**
     * Parses a String into a Binary
     *
     * @param num (String) the number in String format
     * @return (Binary)
     */
    public static Binary parseString (String num) {
        return new Binary(num);
    }

    /**
     * Formats the Binary as String
     *
     * @return (String)
     */
    public String toString () {
        return format(this, 8);
    }

    /**
     * Returns the Binary as Integer
     *
     * @return (int)
     */
    public int getNumber() {
        return number;
    }

    /**
     * Sets the Binary Number
     *
     * IMPORTANT!!: put 0b or 0B before the number to set it as binary
     *
     * @param number (int) value of the binary (Start with 0b or 0B)
     */
    public void setNumber(int number) {
        this.number = number;
    }
}
