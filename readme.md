## Binary

*A Java Library to Work with Binaries.*

### Usage:

To create a new Binary:
> Binary binary = new Binary("00000000");	// Binary can be defined with an String
> Binary binary = new Binary(0b00000000);	// Binary can be defined with an Integer (starting with 0b or 0B)
> Binary binary = new Binary();			// Binary Initialices with 00000000

To do a XOR:
> Binary.xor((Binary) num1, (Binary) num2);

To do a OR:
> Binary.or((Binary) num1, (Binary) num2);

To do a AND:
> Binary.and((Binary) num1, (Binary) num2);

To do a LEFT:
> Binary.left((Binary) num, (int) possitions\_to\_move);

To do a RIGHT:
> Binary.right((Binary) num, (int) possitions\_to\_move);

To do a RIGHT Filling with ZEROS:
> Binary.rightZeroFill((Binary) num, (int) possitions\_to\_move);

To do a FLIP:
> Binary.flip((Binary) num);

To format as String:
> Binary.format((Binary) num, (int) size);

